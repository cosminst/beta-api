const args = require('process.args')()[__filename];
const mongo = require('./services/mongoService');
const fixtures = require('./fixtures/index');
const helpers = require('./helpers');

mongo().then(async mongo => {
    const insertPromises = Object.keys(fixtures).map(async collection => {
        if (args[collection]) {
            const coll = mongo.collection(collection);

            let data = fixtures[collection].data;

            if (fixtures[collection].helpers) {
                const fixtureHelpers = fixtures[collection].helpers
                    .filter(helper => helpers.hasOwnProperty(helper.name))
                    .map(helper => ({
                        apply: helpers[helper.name],
                        params: helper.params
                    }));

                data = data.map(async entry => {
                    return await fixtureHelpers.reduce(async (acc, helper) => {
                        return helper.apply(await acc, helper.params);
                    }, entry);
                });

                data = await Promise.all(data);
            }

            return insert(coll, data, args.clear);
        } else {
            return Promise.resolve();
        }
    });

    return Promise.all(insertPromises);
}).then(() => {
    // nothing
}).catch(err => {
    console.log(err);
}).then(() => {
    process.exit(0);
});

const insert = (coll, data, withDelete = false) => {
    if (withDelete) {
        return coll.deleteMany({}).then(() => {
            return coll.insertMany(data);
        });
    } else {
        return coll.insertMany(data);
    }
};