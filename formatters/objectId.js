const ObjectId = require('mongodb').ObjectId;

module.exports = value => {
    if (value instanceof ObjectId) {
        return value;
    } else {
        return ObjectId(value);
    }
};