const httpError = require('./httpError');

module.exports = resource => {
    return {
        ...httpError(404, 'ERR_NOT_FOUND'),
        resource
    };
};