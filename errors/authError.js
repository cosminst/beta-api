const httpError = require('./httpError');

module.exports = error => {
    return {
        ...httpError(401, error)
    };
};