const httpError = require('./httpError');

module.exports = error => {
    return {
        ...httpError(RESPONSE.ACCESS_DENIED, error || 'ERR_ACCESS_DENIED')
    };
};