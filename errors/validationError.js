const httpError = require('./httpError');

module.exports = errors => {
    return {
        ...httpError(400, 'ERR_VALIDATION_FAILED'),
        errors
    };
};