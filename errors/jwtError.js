const httpError = require('./httpError');

const ERROR_TYPES = {
    TokenExpiredError: 'ERR_JWT_EXPIRED',
    JsonWebTokenError: 'ERR_JWT_INVALID'
};

module.exports = (error) => {
    return httpError(401, ERROR_TYPES[error.name] || ERROR_TYPES.JsonWebTokenError);
};