const jwt = require('jsonwebtoken');
const parameters = require('../config/parameters');

const SECRET_KEY = 'secret';

const expiryFormula = ttl => Math.floor(Date.now() / 1000) + ttl;

module.exports = payload => ({
    async verify(token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, SECRET_KEY, (err, decoded) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(decoded);
                }
            });
        });
    },

    async generate() {
        return new Promise((resolve, reject) => {
            jwt.sign({
                email: payload.user.get('email'),
                exp: payload.ttl || expiryFormula(parameters.jwtExpiration)
            }, SECRET_KEY, (err, token) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(token);
                }
            });
        });
    },
});