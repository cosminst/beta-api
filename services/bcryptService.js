const bcrypt = require('bcrypt');
const authError = require('../errors/authError');

const SALT_ROUNDS = 10;

module.exports = plain => ({
    /**
     * Hashes a string using the bcrypt algorithm
     * @return {Promise<string>} - promise fulfilled when hashing is over; receives the hash as parameter
     */
    async hash() {
        return bcrypt.hash(plain, SALT_ROUNDS);
    },

    /**
     * Hashes a string using the bcrypt algorithm synchronously
     * @return {string} - the hashed string
     */
    hashSync() {
        return bcrypt.hashSync(plain, SALT_ROUNDS);
    },

    /**
     * Compares a hash to the given plain string
     * @param {string} hash - the hash to compare the plain string with
     * @return {Promise} - resolved if the comparison is successful
     */
    async compare(hash) {
        return bcrypt.compare(plain, hash).then(allowed => {
            if (allowed) {
                return Promise.resolve();
            } else {
                return Promise.reject(authError('ERR_INVALID_PASSWORD'));
            }
        });
    }
});