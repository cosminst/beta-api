const Rentals = require('../collections/Rentals');

module.exports = () => ({
    /**
     * Gets a library that has this book ready
     * @param {Book|Model} book - the book model
     * @return {Promise<Library>}
     */
    async getAvailableLibrary(book) {
        return new Rentals().findBookRentals(book)
            .then(rentals => {
                const library = book.get('libraries').find(lib => {
                    return lib.get('instances') > rentals.count(rnt => {
                        return rnt.get('library').toString() === lib.getIdentifier().toString();
                    });
                });

                return library || null;
            });
    }
});