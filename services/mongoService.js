const MongoClient = require('mongodb').MongoClient;
const parameters = require('../config/parameters');

let instance = null;

module.exports = () => {
    // noinspection JSCheckFunctionSignatures
    return instance ? instance :
        MongoClient.connect(parameters.mongoString, {useNewUrlParser: true})
            .then(database => instance = database.db('beta'));
};