const validators = require('../validators');

module.exports = (ctx) => ({
    /**
     * Validates a given value against a set of validators
     * @param {*} value - the value to be validated
     * @param {object|Array} validators - the set of validators
     * @return {Promise} - fulfilled when validation is finished
     */
    async validate(value, validators) {
        if (Array.isArray(validators)) {
            const appliedValidators = validators.map(validator => this.applyValidator(value, validator));

            return Promise.all(appliedValidators)
                .then(errors => errors.filter(e => e))
                .then(errors => errors.length === 0 ? null : errors);
        } else {
            return this.validateObject(value, validators).then(errors => {
                if (this.hasNestedErrors(errors)) {
                    throw errors;
                } else {
                    return null;
                }
            });
        }
    },

    /**
     * Validates a given object key by key against a set of validators
     * @private
     * @param {object} object - the object to be validated
     * @param {object} validators - the set of validators
     * @return {Promise} - promise fulfilled when all the object keys have been validated
     */
    async validateObject(object, validators) {
        object = object || {};
        const validatedKeys = Object.keys(validators).map(key => {
            return this.validate(object[key], validators[key]).catch(failedValidator => {
                delete failedValidator.validate;
                return {
                    [key]: failedValidator,
                };
            });
        });

        return Promise.all(validatedKeys).then(invalidArray => {
            return invalidArray.reduce((acc, fieldErrors) => {
                return Object.assign(acc, fieldErrors);
            }, {});
        });
    },

    /**
     * Tests a value against a certain validator
     * @private
     * @param {*} value - the value to be validated
     * @param {string|{name: (string), params: (object)}} validator
     * @return {Promise} - promise fulfilled if the validation passed
     */
    async applyValidator(value, validator) {
        let constraint = null;

        switch (typeof validator) {
            case "string":
                constraint = validator;
                break;
            case "object":
                constraint = validator.name;
                break;
        }

        const validatorInstance = validators[constraint](validator.params);
        return validatorInstance.validate(value, ctx);
    },

    /**
     * @private
     * Checks for nested errors in an object validation result
     * @param {object} object - the object to check if empty
     * @return {boolean}
     */
    hasNestedErrors(object) {
        return Object.keys(object).some(key => {
            if (typeof object[key] === "object") {
                return this.hasNestedErrors(object[key]);
            } else {
                return !!object[key];
            }
        });
    }
});