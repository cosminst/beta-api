const Rentals = require('../collections/Rentals');

module.exports = () => ({
    /**
     * Finds the stock for a book
     * @param {Book|Model} book - the book to find the stock for
     * @return {Promise<number>}
     */
    async getBookStock(book) {
        const rentals = await new Rentals().findBookRentals(book, Rentals.OPEN);

        return book.getTotalStock() - rentals.length;
    },

    /**
     * Retrieves the list of rented books
     * @param {User} rentee - the rentee
     * @return {Promise<Rentals>}
     */
    async getRentedBooks(rentee) {
        return new Rentals().filter({rentee: rentee.getIdentifier()});
    }
});