module.exports = () => ({
    grant(user, flags) {
        const flg = Array.from(user.get('accessFlags') + flags)
            .sort()
            .filter((flag, idx, self) => self.indexOf(flag) === idx)
            .join('');

        user.set('accessFlags', flg);
        return user.save();
    }
});