module.exports = {
    users: require('./users.json'),
    memberships: require('./memberships.json'),
    books: require('./books.json'),
    libraries: require('./libraries.json'),
};