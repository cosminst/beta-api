module.exports = () => ({
    newMembership: {
        city: ['notEmpty'],
        county: ['notEmpty'],
        street: ['notEmpty'],
        streetNumber: ['notEmpty'],
    }
});