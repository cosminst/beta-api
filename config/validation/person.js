module.exports = () => ({
    newMembership: {
        firstName: [
            'notEmpty',
            {
                name: 'length',
                params: {
                    min: 3
                }
            }
        ],
        lastName: [
            'notEmpty',
            {
                name: 'length',
                params: {
                    min: 3
                }
            }
        ],
        nationalIdentityNumber: ['notEmpty', 'nin'],
        birthDate: [
            'notEmpty',
            {
                name: 'regexp',
                params: {
                    pattern: /\d{4}-\d{2}-\d{2}/
                }
            }
        ]
    }
});