const Membership = require('../../models/Membership');

module.exports = () => ({
    login: {
        email: ['notEmpty', 'isEmail'],
        password: ['notEmpty']
    },

    newMembership: {
        email: ['notEmpty', 'isEmail', 'existingEmail'],
        password: ['notEmpty'],
        membershipType: [
            'notEmpty',
            {
                name: 'choice',
                params: {
                    choices: [
                        Membership.FREEMIUM,
                        Membership.MEMBER,
                        Membership.PREMIUM
                    ]
                }
            }
        ]
    }
});