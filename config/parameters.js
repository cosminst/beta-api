module.exports = {
    mongoString: process.env.MONGO_STRING || 'mongodb://127.0.0.1/beta',
    jwtExpiration: 3600,
    emailPattern: /^[^.\s](\.?[\w_\-#^+$%&|'{}!?*=]*)+@[\w\-_]+(\.\w{2,})*$/,
};