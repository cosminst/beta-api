module.exports = {
    NO_LIMIT: 0,
    NO_OFFSET: 0,
    DEFAULT_LIMIT: 0,
    DEFAULT_OFFSET: 0,
};