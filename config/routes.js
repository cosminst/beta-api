module.exports = {
    AUTHORIZATION: '/auth',
    MEMBERSHIPS: '/memberships',
    MEMBERSHIP_PLANS: '/memberships',
    BOOKS: {
        GET: '/books',
        CREATE: '/books',
        SINGLE: '/books/:isbn([A-Za-z0-9\-]{11,})',
        EDIT: '/books/:isbn([A-Za-z0-9\-]{13})',
        DELETE: '/books/:isbn([A-Za-z0-9\-]{13})'
    },
    LIBRARIES: {
        GET: '/libraries',
        CREATE: '/libraries',
        SINGLE: '/libraries/:id([a-f0-9]{24})',
        EDIT: '/libraries/:id([a-f0-9]{24})'
    },
    RENTALS: {
        GET: '/rentals',
        CREATE: '/rentals',
        EDIT: '/rentals/:id([a-f0-9]{24})',
    }
};