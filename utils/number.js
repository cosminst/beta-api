module.exports = () => ({

    /**
     * Checks whether the given number is a number
     * @return {boolean}
     */
    isNumber(number) {
        return /^[+-]?\d*$/g.test(number);
    }
});