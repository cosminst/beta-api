const Model = require('./Model');
const Library = require('./Library');
const Libraries = require('../collections/Libraries');
const string = require('../formatters/string');

class Book extends Model {

    /**
     * @inheritDoc
     */
    get identifier() {
        return 'isbn';
    }

    /**
     * @inheritDoc
     */
    getFormatters() {
        return {
            isbn: string
        };
    }

    /**
     * Adds a library
     * @param {Library} lib - the library to be added
     * @param {number} instances - the number of instances
     * @return {this}
     */
    addStock(lib, instances) {
        instances = Number(instances);
        const currentLibraries = this.get('libraries') || new Libraries();
        const libStock = currentLibraries.find(e => e.getIdentifier() === lib.getIdentifier());

        if (libStock) {
            libStock.instances += instances;
            return this;
        } else {
            currentLibraries.push(lib.set('instances', instances));

            return this.set('libraries', currentLibraries);
        }
    }

    /**
     * @inheritDoc
     */
    async fetch() {
        return super.fetch()
            .then(book => {
                const hydrateLibraries = book.get('libraries').map(lib => lib.fetch());
                return Promise.all(hydrateLibraries);
            })
            .then(() => this);
    }

    getPersistedData() {
        const data = this.json();

        return {
            ...data,
            libraries: this.get('libraries') ? this.get('libraries').map(lib => ({
                library: lib.getIdentifier(),
                instances: lib.get('instances')
            })) : []
        };
    }

    /**
     * @inheritDoc
     */
    parse(data) {
        const parsedData = Object.assign({}, data);
        parsedData.libraries = data.libraries.map(library => {
            return new Library({instances: library.instances})
                .setIdentifier(library.library);
        });

        parsedData.libraries = new Libraries(parsedData.libraries);

        return parsedData;
    }

    /**
     * Gets the total stock of this book
     * @return {number}
     */
    getTotalStock() {
        return this.get('libraries').count(lib => lib.get('instances'));
    }
}

Book.collection = 'books';

module.exports = Book;