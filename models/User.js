const Model = require('./Model');
const validators = require('../config/validation/user');
const resourceNotFoundError = require('../errors/resourceNotFoundError');

class User extends Model {
    /**
     * Retrieves the validators for this model
     * @return {{newMembership, login}}
     */
    static get validators() {
        return validators();
    }

    /**
     * Mongo collection to use for this model
     * @return {string}
     */
    static get collection() {
        return 'users';
    }

    /**
     * Retrieves a user by his email address
     * @param {string} email - the email address to look after
     * @return {Promise<User>}
     */
    async findByEmail(email) {
        const userData = await this.getMongo().findOne({email});

        if (userData) {
            this.assign(this.parse(userData));
        } else {
            throw resourceNotFoundError({
                type: this.constructor.name,
                email
            });
        }

        return this;
    }

    /**
     * Checks whether this user has a certain access flag set
     * @param {string} flag - the flag to check for
     * @return {boolean}
     */
    hasAccessFlag(flag) {

    }

    /**
     * @inheritDoc
     */
    json() {
        delete this.data.passwordHash;
        delete this.data.email;
        return super.json();
    }
}

module.exports = User;