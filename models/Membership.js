const Model = require('./Model');

class Membership extends Model {
    /**
     * The collection associated with this model
     * @return {string}
     */
    static get collection() {
        return 'memberships';
    }
}

Membership.FREEMIUM = 1;
Membership.MEMBER = 2;
Membership.PREMIUM = 3;

module.exports = Membership;