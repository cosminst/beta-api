const Collection = require('../collections/Collection');
const resourceNotFoundError = require('../errors/resourceNotFoundError');
const mongo = require('../services/mongoService');
const objectId = require('../formatters/objectId');
const ObjectId = require('mongodb').ObjectId;

class Model {

    /**
     * Retrieves this model's identifier
     * @return {string}
     */
    get identifier() {
        return '_id';
    }
    
    /**
     * @constructor
     * @param {object} data - the model data
     */
    constructor(data = null) {
        this.data = data ? this.parse(data) : {};
        this.set(this.identifier, new ObjectId(), false);
    }

    /**
     * Parse the given data
     * @param {Object} data - the data to be parsed
     * @return {Object} - the parsed object
     */
    parse(data) {
        return data;
    }

    /**
     * Returns this model's identifier
     * @return {string}
     */
    getIdentifier() {
        return this.get(this.identifier);
    }

    /**
     * Sets the identifier to a desired value
     * @param {string} value - the value to use for the identifier
     * @return {Model}
     */
    setIdentifier(value) {
        this.set(this.identifier, value);
        return this;
    }

    /**
     * Retrieves the value from the given key
     * @param {string} key - the key for which to retrieve the value
     * @return {*} - the value of the given key
     */
    get(key) {
        const value = this.data[key];

        if (typeof value === 'undefined') {
            return null;
        } else {
            return value;
        }
    }

    /**
     * Sets a value to the given key
     * @param {string} key - the key where to set the value
     * @param {*} value - the value to be set
     * @param {boolean} overwrite - overwrite the old value if it exists
     * @return {Model}
     */
    set(key, value, overwrite = true) {
        if (overwrite || !this.data.hasOwnProperty(key)) {
            const formatters = this.getFormatters();
            if (formatters.hasOwnProperty(key)) {
                this.data[key] = formatters[key](value);
            } else {
                this.data[key] = value;
            }
        }

        return this;
    }

    /**
     * Returns the formatters to be used when setting a value
     * @return {Object}
     */
    getFormatters() {
        return {
            [this.identifier]: objectId
        };
    }

    /**
     * Assigns an object to this model
     * @param {Object} object - object to be assigned to this model's data
     * @return {Model}
     */
    assign(object) {
        Object.keys(object).forEach(key => {
            const value = object[key];
            this.set(key, value);
        });

        return this;
    }

    /**
     * Loads this model from the database and updates it
     * @return {Promise<Model>}
     */
    async fetch() {
        return this.getMongo()
            .findOne({[this.identifier]: this.getIdentifier()})
            .then(data => {
                if (data) {
                    this.assign(this.parse(data));
                    return this;
                } else {
                    throw resourceNotFoundError({
                        type: this.constructor.name,
                        [this.identifier]: this.getIdentifier()
                    });
                }
            });
    }

    /**
     * Saves this model to the database
     * @return {Promise<Model>}
     */
    async save() {
        return this.getMongo().update({
            [this.identifier]: this.getIdentifier()
        }, this.getPersistedData(), {upsert: true}).then(() => {
            return this;
        });
    }

    /**
     * Returns the mongo service for this collection
     * @return {*}
     */
    getMongo() {
        return mongo().collection(this.constructor.collection);
    }

    /**
     * Returns a model's persisted data
     * @return {Object}
     */
    getPersistedData() {
        return Object.keys(this.data).reduce((data, key) => {
            const value = this.data[key];
            if (value instanceof Model) {
                data[key] = value.getIdentifier();
            } else if (value instanceof Collection) {
                data[key] = value.map(model => model.getIdentifier());
            } else {
                data[key] = value;
            }

            return data;
        }, {});
    }

    /**
     * Transforms this model in a JSON object
     * @return {object}
     */
    json() {
        return Object.keys(this.data).reduce((json, key) => {
            const value = this.data[key];
            if (value instanceof Model || value instanceof Collection) {
                json[key] = value.json();
            } else {
                json[key] = value;
            }

            return json;
        }, {});
    }
}

module.exports = Model;