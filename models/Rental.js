const Model = require('./Model');
const Book = require('./Book');
const Library = require('./Library');
const User = require('./User');

class Rental extends Model {

    /**
     * Gets the rentals collection
     * @return {string}
     */
    static get collection() {
        return 'rentals';
    }

    /**
     * @inheritDoc
     */
    parse(data) {
        const parsedData = Object.assign({}, data);

        parsedData.book = new Book().setIdentifier(data.book);
        parsedData.rentee = new User().setIdentifier(data.rentee);
        parsedData.library = new Library().setIdentifier(data.library);

        return parsedData;
    }
}

Rental.NEW = 'new';
Rental.CANCELLED = 'cancelled';
Rental.TO_BE_DELIVERED = 'to_be_delivered';
Rental.DELIVERED = 'delivered';
Rental.DUE = 'due';
Rental.CLOSED = 'closed';
Rental.OPEN = [Rental.NEW, Rental.TO_BE_DELIVERED, Rental.DELIVERED, Rental.DUE];
Rental.RESOLVED = [Rental.CANCELLED, Rental.CLOSED];

module.exports = Rental;