const Model = require('./Model');

class Library extends Model {
    /**
     * Collection to be used with this model
     * @return {string}
     */
    static get collection() {
        return 'libraries';
    }
}

module.exports = Library;