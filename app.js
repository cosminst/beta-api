const express = require('express');
const bodyParser = require('body-parser');
const requestParser = require('./middlewares/requestParser');
const errorHandler = require('./middlewares/errorHandler');
const mongo = require('./services/mongoService');
const cors = require('cors');

global.ROUTE = require('./config/routes');
global.RESPONSE = require('./config/responseStatus');
global.CONSTANTS = require('./config/constants');
const registerRoutes = require('./routes');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(requestParser());
registerRoutes(app);
app.use(errorHandler());

mongo().then(() => {
    app.listen(8888, 'localhost');
});
