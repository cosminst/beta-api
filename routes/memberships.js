const express = require('express');
const router = express.Router();
const requestValidator = require('../middlewares/requestValidator');
const bcryptService = require('../services/bcryptService');

const Memberships = require('../collections/Memberships');
const User = require('../models/User');

const userValidators = require('../config/validation/user')();
const personalDataValidators = require('../config/validation/person')();
const addressValidators = require('../config/validation/address')();

const registerValidation = {
    ...userValidators.newMembership,
    person: {
        ...personalDataValidators.newMembership,
        address: addressValidators.newMembership
    }
};

// @GET /memberships
// Used for retrieving all available membership plans
router.get(ROUTE.MEMBERSHIP_PLANS, (req, res, next) => {
    const memberships = new Memberships();
    memberships.filter().then(() => {
        res.status(RESPONSE.OK).send(memberships.json(true));
    }).catch(next);
});

// @POST /memberships
// Used for registering new members to the platform
router.post(ROUTE.MEMBERSHIPS, requestValidator(registerValidation), (req, res, next) => {
    bcryptService(req.body.password).hash().then(encrypted => {
        req.body.passwordHash = encrypted;
        delete req.body.password;
        return new User(req.body).save();
    }).then(() => {
        res.status(RESPONSE.CREATED).send();
    }).catch(next);
});

module.exports = router;