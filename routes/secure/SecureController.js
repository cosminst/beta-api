class SecureController {
    /**
     * @constructor
     */
    constructor() {
        this._get = [];
        this._post = [];
        this._put = [];
        this._delete = [];
    }

    /**
     * Add a get route
     * @param {string} path - the path
     * @param {function|function[]} handle - the handler
     * @return {SecureController}
     */
    get(path, ...handle) {
        this._get.push({path, handle});
        return this;
    }

    /**
     * Add a post route
     * @param {string} path - the path
     * @param {function|function[]} handle - the handler
     * @return {SecureController}
     */
    post(path, ...handle) {
        this._post.push({path, handle});
        return this;
    }

    /**
     * Add a put route
     * @param {string} path - the path
     * @param {function|function[]} handle - the handler
     * @return {SecureController}
     */
    put(path, ...handle) {
        this._put.push({path, handle});
        return this;
    }

    /**
     * Add a delete route
     * @param {string} path - the path
     * @param {function|function[]} handle - the handler
     * @return {SecureController}
     */
    delete(path, ...handle) {
        this._delete.push({path, handle});
        return this;
    }

    /**
     * Returns the array of GET paths
     * @return {[]|Array}
     */
    gets() {
        return this._get;
    }

    /**
     * Returns the array of POST paths
     * @return {[]|Array}
     */
    posts() {
        return this._post;
    }

    /**
     * Returns the array of PUT paths
     * @return {[]|Array}
     */
    puts() {
        return this._put;
    }

    /**
     * Returns the array of DELETE paths
     * @return {[]|Array}
     */
    deletes() {
        return this._delete;
    }
}

module.exports = SecureController;