const SecureController = require('./SecureController');
const Users = require('../../collections/Users');

module.exports = new SecureController()
    .get('/users', (req, res, next) => {
        new Users()
            .filter()
            .then(users => {
                res.status(RESPONSE.OK).send(users.json(true));
            })
            .catch(next);
    });