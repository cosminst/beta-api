const SecureController = require('./SecureController');
const accessValidator = require('../../middlewares/accessValidator');
const requestValidator = require('../../middlewares/requestValidator');
const Library = require('../../models/Library');
const Libraries = require('../../collections/Libraries');
const httpError = require('../../errors/httpError');
const Books = require('../../collections/Books');
const User = require('../../models/User');
const userService = require('../../services/userService');
const base64 = require('base-64');
const csvtojson = require('csvtojson');
const Book = require('../../models/Book');
const utf8 = require('utf8');

const libraryValidators = {};

module.exports = new SecureController()
    .post(
        ROUTE.LIBRARIES.CREATE,
        accessValidator('d'),
        requestValidator(libraryValidators),
        (req, res, next) => {
            new Libraries()
                .filter({name_canonical: req.body.name_canonical})
                .then(libs => {
                    if (libs.length > 0) {
                        throw httpError(RESPONSE.BAD_REQUEST, 'ERR_DUPLICATE_IDENTIFIER');
                    } else {
                        return new Library(req.body).save();
                    }
                })
                .then(lib => res.status(RESPONSE.CREATED).send(lib))
                .catch(next);
        }
    )

    .put(
        ROUTE.LIBRARIES.EDIT,
        accessValidator('d'),
        requestValidator(libraryValidators),
        (req, res, next) => {
            new Library().setIdentifier(req.params.id)
                .fetch()
                .then(model => {
                    if (req.body.librarian) {
                        model.set('librarian', req.body.librarian);
                        new User().setIdentifier(req.body.librarian)
                            .fetch()
                            .then(user => userService().grant(user, 'abefi'))
                            .catch(next);
                    }

                    return model.save();
                })
                .then(() => res.status(RESPONSE.EMPTY).send())
                .catch(next);
        }
    )

    .get(
        '/my-library',
        accessValidator('efi'),
        (req, res, next) => {
            new Libraries().filter({librarian: req.author.getIdentifier().toString()})
                .then(collection => {
                    res.status(200).send(collection.at(0).json());
                })
                .catch(next);
        }
    )

    .post(
        '/books/import',
        accessValidator('e'),
        (req, res, next) => {
            const data = req.body.data.split(/[;,]/).pop();
            const bytes = base64.decode(data);
            const importData = utf8.decode(bytes);

            csvtojson().fromString(importData).subscribe(json => {
                new Book().setIdentifier(json.isbn)
                    .fetch()
                    .then(book => book)
                    .catch(() => new Book().setIdentifier(json.isbn))
                    .then(book => {
                        return new Libraries().filter({librarian: req.author.getIdentifier().toString()})
                            .then(coll => coll.at(0))
                            .then(lib => {
                                book.set('title', json.title);
                                book.set('author', json.author);
                                book.set('synopsis', json.synopsis);
                                book.set('year', json.year);
                                book.addStock(lib, json.instances);
                                return book.save();
                            });
                    });
            }).then(() => {
                res.status(RESPONSE.CREATED).send();
            }).catch(next);
        }
    )

    .get(
        ROUTE.LIBRARIES.GET,
        (req, res, next) => {
            new Libraries()
                .filter(req.query)
                .then(collection => {
                    const prms = collection.map(lib => new Books().countByLibrary(lib));
                    const prmsLibrarian = collection.map(lib => {
                        if (lib.get('librarian')) {
                            return new User().setIdentifier(lib.get('librarian')).fetch();
                        } else {
                            return Promise.resolve(null);
                        }
                    });

                    return Promise.all(prms).then(counts => {
                        collection.map((lib, idx) => lib.set('bookCount', counts[idx]));
                        return Promise.all(prmsLibrarian);
                    }).then(librarians => {
                        collection.map((lib, idx) => lib.set('librarian', librarians[idx]));
                        res.status(RESPONSE.OK).send(collection.json(true));
                    });
                })
                .catch(next);
        }
    );