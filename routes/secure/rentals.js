const SecureController = require('./SecureController');
const rentalService = require('../../services/rentalService');
const requestValidator = require('../../middlewares/requestValidator');
const Rental = require('../../models/Rental');
const Book = require('../../models/Book');
const httpError = require('../../errors/httpError');
const bookService = require('../../services/bookService');
const accessValidator = require('../../middlewares/accessValidator');
const Rentals = require('../../collections/Rentals');

const rentalValidators = {};

module.exports = new SecureController()
    .get(
        ROUTE.RENTALS.GET,
        (req, res, next) => {
            rentalService().getRentedBooks(req.author)
                .then(collection => {
                    const prms = collection.map(r => {
                        if (r.get('status') === Rental.DELIVERED) {
                            const dueDate = new Date(r.get('dueDate'));
                            if (dueDate < new Date()) {
                                r.set('status', Rental.DUE);
                                return r.save();
                            } else {
                                return Promise.resolve(r);
                            }
                        } else {
                            return Promise.resolve(r);
                        }
                    });

                    return Promise.all(prms);
                })
                .then(arr => new Rentals(arr))
                .then(collection => res.status(RESPONSE.OK).send(collection.json(true)))
                .catch(next);
        })

    .post(
        ROUTE.RENTALS.CREATE,
        accessValidator('b'),
        requestValidator(rentalValidators),
        (req, res, next) => {
            const rental = new Rental()
                .set('status', Rental.DELIVERED)
                .set('book', req.body.book)
                .set('rentee', req.author.getIdentifier())
                .set('dueDate', new Date().setDate(new Date().getDate() + 14));

            new Book().setIdentifier(rental.get('book'))
                .fetch()
                .then(book => {
                    const stock = rentalService().getBookStock(book);
                    if (stock <= 0) {
                        throw httpError(RESPONSE.BAD_REQUEST, 'ERR_OUT_OF_STOCK');
                    } else {
                        return bookService().getAvailableLibrary(book);
                    }
                })
                .then(library => rental.set('library', library))
                .then(rental => rental.save())
                .then(() => res.status(RESPONSE.CREATED).send())
                .catch(next);
        })

    .put(
        ROUTE.RENTALS.EDIT,
        accessValidator('bc'),
        requestValidator(rentalValidators),
        (req, res, next) => {
            new Rental().setIdentifier(req.params.id)
                .fetch()
                .then(rental => rental
                    .set('book', req.body.book)
                    .set('status', req.body.status)
                    .save())
                .then(() => res.status(RESPONSE.EMPTY).send())
                .catch(next);
        });
