const express = require('express');
const requestAuthor = require('../../middlewares/requestAuthor');
const tokenValidator = require('../../middlewares/tokenValidator');

const controllers = [
    require('./rentals'),
    require('./libraries'),
    require('./users'),
    require('./books'),
];

const securityMiddlewares = () => [
    tokenValidator(),
    requestAuthor()
];

module.exports = controllers.map(c => {
    const router = express.Router();

    // noinspection JSCheckFunctionSignatures
    c.gets().forEach(get => router.get(get.path, ...securityMiddlewares(), ...get.handle));

    // noinspection JSCheckFunctionSignatures
    c.posts().forEach(post => router.post(post.path, ...securityMiddlewares(), ...post.handle));

    // noinspection JSCheckFunctionSignatures
    c.puts().forEach(put => router.put(put.path, ...securityMiddlewares(), ...put.handle));

    // noinspection JSCheckFunctionSignatures
    c.deletes().forEach(del => router.delete(del.path, ...securityMiddlewares(), ...del.handle));

    return router;
});