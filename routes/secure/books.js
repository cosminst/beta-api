const Books = require('../../collections/Books');
const Book = require('../../models/Book');
const rentalService = require('../../services/rentalService');
const SecureController = require('./SecureController');

module.exports = new SecureController()
    .get(
        ROUTE.BOOKS.GET,
        (req, res, next) => {
            new Books().filter(req.query).then(collection => {
                res.status(RESPONSE.OK).send(collection.json(true));
            }).catch(next);
        }
    )
    .get(
        ROUTE.BOOKS.SINGLE,
        (req, res, next) => {
            new Book()
                .setIdentifier(req.params.isbn)
                .fetch()
                .then(async book => {
                    const stock = await rentalService().getBookStock(book);
                    const userRentals = await rentalService().getRentedBooks(req.author);
                    book.set('stock', stock);
                    book.set('hasRental', stock < book.getTotalStock() && userRentals.length > 0);
                    res.status(RESPONSE.OK).send(book.json());
                }).catch(next);
        }
    );
