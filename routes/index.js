const authorization = require('./authorization');
const memberships = require('./memberships');

const secureRoutes = require('./secure');

module.exports = app => {
    app.use(authorization);
    app.use(memberships);

    secureRoutes.forEach(r => {
        app.use(r);
    });
};