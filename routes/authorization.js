const express = require('express');
const requestValidator = require('../middlewares/requestValidator');
const userValidators = require('../config/validation/user');
const User = require('../models/User');
const bcryptService = require('../services/bcryptService');
const jwtService = require('../services/jwtService');

const router = express.Router();
const loginConstraints = userValidators().login;

router.post(ROUTE.AUTHORIZATION, requestValidator(loginConstraints), (req, res, next) => {
    new User().findByEmail(req.body.email).then(user => {
        return bcryptService(req.body.password).compare(user.get('passwordHash')).then(() => {
            return jwtService({user}).generate();
        }).then(token => {
            return Object.assign({token}, {
                [user.identifier]: user.getIdentifier(),
                firstName: user.get('person').firstName,
                lastName: user.get('person').lastName,
                email: user.get('email'),
                accessFlags: user.get('accessFlags')
            });
        });
    }).then(data => {
        res.status(RESPONSE.OK).send(data);
    }).catch(next);
});

module.exports = router;