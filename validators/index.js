module.exports = {
    notEmpty: require('./notEmpty'),
    isEmail: require('./isEmail'),
    choice: require('./choice'),
    length: require('./length'),
    nin: require('./nin'),
    regexp: require('./regexp'),
    existingEmail: require('./existingEmail'),
};
