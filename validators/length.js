const notEmpty = require('./notEmpty');

module.exports = (params, ctx) => ({
    message: 'ERR_VALIDATION_LENGTH',
    options: params,

    /**
     * Validates that the given value has a certain length
     * @param {string|Array} value - the value we want to check the length of
     * @return {Promise} - resolved if the value's length is valid
     */
    async validate(value) {
        try {
            await notEmpty().validate(value);
        } catch (e) {
            return Promise.resolve();
        }

        const {min, max} = params;

        if ((min && value.length < min) || (max && value.length > max)) {
            return Promise.reject(this);
        }
    }
});