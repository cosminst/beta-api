const notEmpty = require('./notEmpty');
const User = require('../models/User');

module.exports = (params, ctx) => ({
    message: 'ERR_VALIDATION_EXISTING_EMAIL',

    /**
     * Checks whether an email exists or not
     * @param {string} email - the email to be searched for
     * @return {Promise} - resolved if the email does not exist
     */
    async validate(email) {
        try {
            await notEmpty().validate(email);
            await new User().findByEmail(email);
        } catch (e) {
            return Promise.resolve();
        }

        return Promise.reject(this);
    }
});