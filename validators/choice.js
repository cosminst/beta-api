const notEmpty = require('./notEmpty');

module.exports = (params, ctx) => ({
    message: 'ERR_VALIDATION_INVALID_CHOICE',

    /**
     * Validates that a given value is within a choice range
     * @param {string|number} value - the value to be checked
     * @return {Promise} - resolved if the value is included in the choices array
     */
    async validate(value) {
        try {
            await notEmpty().validate(value);
        } catch (e) {
            return Promise.resolve();
        }

        if (!params.choices.includes(value)) {
            return Promise.reject(this);
        }
    }
});