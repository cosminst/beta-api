const regexp = require('./regexp');
const parameters = require('../config/parameters');

module.exports = (params, ctx) => ({
    message: 'ERR_VALIDATION_IS_EMAIL',

    /**
     * Validates that a given string is a valid email
     * @param {string} value - the value to be validated
     * @return {Promise} - promise resolved if the given string is a valid email
     */
    async validate(value) {
        return regexp({pattern: parameters.emailPattern}).validate(value)
            .catch(() => {
                return Promise.reject(this);
            });
    }
});