module.exports = (params, ctx) => ({
    message: 'ERR_VALIDATION_INVALID_NIN',

    /**
     * Validates that a national identity number is valid for the country provided on context
     * @param {string} value - the national identity number to be tested
     * @return {Promise} - resolved if the value is a valid NIN
     */
    async validate(value) {
        return Promise.resolve();
    }
});