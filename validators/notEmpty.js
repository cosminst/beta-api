module.exports = () => ({
    message: 'ERR_VALIDATION_NOT_EMPTY',

    /**
     * Validates that a value is not empty
     * @param {*} value - the value to be checked
     * @return {Promise} - rejected if the passed value is considered empty
     */
    async validate(value) {
        if (typeof value === "undefined" || value === null || value.length === 0) {
            return Promise.reject(this);
        } else {
            return Promise.resolve();
        }
    }
});