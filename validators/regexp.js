const notEmpty = require('./notEmpty');

module.exports = (params, ctx) => ({
    message: 'ERR_VALIDATION_WRONG_FORMAT',

    /**
     * Tests the given value against a certain regexp pattern
     * @param {string} value - the value to be tested
     * @return {Promise} - resolved if the value matches the pattern
     */
    async validate(value) {
        try {
            await notEmpty().validate(value);
        } catch (e) {
            return Promise.resolve();
        }

        if (!params.pattern.test(value)) {
            return Promise.reject(this);
        }
    }
});