const bcryptService = require('../services/bcryptService');

/**
 * Helper that encrypts a certain field in a data fixture using the bcryptService
 * @param {Object} entry - the fixture entry
 * @param {Object} params - helper parameters
 * @return {Object} - the modified entry object
 */
module.exports = (entry, params) => {
    const {field, postEncryptionField, deleteOld} = params;
    return bcryptService(entry[field]).hash().then(encrypted => {
        entry[postEncryptionField] = encrypted;
        if (deleteOld) {
            delete entry[field];
        }

        return entry;
    });
};