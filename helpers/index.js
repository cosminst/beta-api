module.exports = {
    encrypt: require('./encryptionHelper'),
    dependency: require('./dependencyHelper'),
    manyToMany: require('./manyToManyHelper'),
    loripsum: require('./loripsumHelper'),
};