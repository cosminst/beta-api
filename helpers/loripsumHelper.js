const loripsum = require('loripsum');

/**
 * Adds lorem ipsum to the entry
 * @param {Object} entry - the database entry
 * @param {Object} params - the helper parameters
 * @return {Promise<Object>}
 */
module.exports = (entry, params) => {
    const {field, paragraphs, length} = params;

    return loripsum.plain({
        paragraphCount: paragraphs,
        paragraphLength: length
    }).then(text => {
        entry[field] = text;
        return entry;
    });
};