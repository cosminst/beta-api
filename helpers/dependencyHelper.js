const mongo = require('../services/mongoService');

/**
 * Fixtures helper that resolves a dependency to an external object
 * @param {Object} entry - the current fixture object
 * @param {Object} params - the helper parameters
 * @return {Promise<Object>}
 */
module.exports = (entry, params) => {
    if (Array.isArray(entry[params.field])) {
        return handleManyToOne(entry, params);
    } else {
        return handleOneToOne(entry, params);
    }
};

/**
 * Handle the case when the entry field is an array of dependencies
 * @param {Object} entry - the entry object
 * @param {Object} params - the helper's params
 * @return {Promise<Object>} - fulfilled when all dependencies have been replaced successfully
 */
const handleManyToOne = (entry, params) => {
    const {field, collection, collectionField, replaceWithField} = params;
    const requiredDependenciesPromise = entry[field].map(joinValue => {
        return mongo().collection(collection).findOne({
            [collectionField]: joinValue
        }).then(result => {
            return result[replaceWithField];
        });
    });

    return Promise.all(requiredDependenciesPromise).then(result => {
        entry[field] = result;
        return entry;
    });
};

/**
 * Handles the case when the entry field's value is a dependency
 * @param {Object} entry - the entry object
 * @param {Object} params - the helper's params
 * @return {Promise} - fulfilled when the dependency has been replaced
 */
const handleOneToOne = (entry, params) => {
    const {field, collection, collectionField, replaceWithField} = params;
    return mongo().collection(collection).findOne({
        [collectionField]: entry[field]
    }).then(result => {
        entry[field] = result[replaceWithField];
        return entry;
    });
};