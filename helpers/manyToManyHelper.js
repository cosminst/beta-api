const mongo = require('../services/mongoService');

/**
 * Handles many to many relationships
 * @param {Object} entry - the database entry
 * @param {Object} params - the helper parameters
 * @return {Promise<Object>}
 */
module.exports = (entry, params) => {
    const {field, dependency, collection, collectionField, replaceWithField} = params;

    const valuesPromise = entry[field].map(value => {
        return mongo().collection(collection).findOne({
            [collectionField]: value[dependency]
        }).then(result => {
            return result[replaceWithField];
        });
    });

    return Promise.all(valuesPromise).then(result => {
        result.map((item, idx) => {
            entry[field][idx][dependency] = item;
        });

        return entry;
    });
};