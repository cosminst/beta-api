const User = require('../models/User');

module.exports = () => (req, res, next) => {
    let token = req.jwt();

    new User().findByEmail(token.email)
        .then(user => {
            req.author = user;
            next();
        }).catch(next);
};