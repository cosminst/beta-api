const number = require('../utils/number');

module.exports = () => (req, res, next) => {
    typeof req.query === 'object' && parse(req.query);
    next();
};

const parse = object => {
    Object.keys(object).forEach(key => {
        const value = object[key];
        if (number().isNumber(value)) {
            object[key] = parseInt(value);
        } else {
            object[key] = value
                .replace(/ăâ/i, 'a')
                .replace(/ş/i, 's')
                .replace(/î/i, 'i')
                .replace(/ţ/i, 't');
        }
    });
};