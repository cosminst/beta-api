const validatorService = require('../services/validatorService');
const validationError = require('../errors/validationError');

module.exports = validators => (req, res, next) => {
    validatorService().validate(req.body, validators)
        .then(next)
        .catch(errs => {
            next(validationError(errs));
        });
};