const accessError = require('../errors/accessError');

/**
 * Validates that a user has specific access flags before allowing him to call the requested endpoint
 * e.g. accessValidator("abc", "def") means that the person must have ANY OF "abc" AND ANY OF "def"
 * accessValidator("a", "b", "c") - the user must have all flags set ("abc")
 * accessValidator("abc") - the user must have any of the passed access flags
 *
 * @param {string} flags - the list of flags that MUST be had in order to access this endpoint
 * @return {Function}
 */
module.exports = (...flags) => (req, res, next) => {
    const hasFlags = flags.reduce((okay, requiredFlags) => {
        return okay && Array.from(requiredFlags).reduce((result, f) => {
            return result || req.author.hasAccessFlag(f);
        });
    });

    if (hasFlags) {
        next();
    } else {
        next(accessError());
    }
};