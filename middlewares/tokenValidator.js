const jwtService = require('../services/jwtService');
const jwtError = require('../errors/jwtError');

module.exports = () => (req, res, next) => {
    let token = req.header('Authorization');

    if (token) {
        token = token.split('Bearer ').pop();
        jwtService().verify(token).then(decoded => {
            req.jwt = () => decoded;
            next();
        }).catch(err => {
            next(jwtError(err));
        });
    } else {
        throw jwtError({});
    }
};