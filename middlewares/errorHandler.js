module.exports =  () => {
    return (err, req, res, next) => {
        if (err.code) {
            res.status(err.code).send(err);
        } else {
            res.status(500).send(err.message);
        }
    };
};