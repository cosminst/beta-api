const Collection = require('./Collection');
const Library = require('../models/Library');

class Libraries extends Collection {

    /**
     * Get the model to use with this collection
     * @return {Library}
     */
    static get model() {
        return Library;
    }
}

module.exports = Libraries;