const Collection = require('./Collection');
const Rental = require('../models/Rental');

class Rentals extends Collection {
    static get model() {
        return Rental;
    }

    /**
     * Returns the open status filter
     * @return {Object}
     */
    static get OPEN() {
        return {
            $and: Rental.RESOLVED.map(status => ({
                status: {$not: {$eq: status}}
            }))
        };
    }

    /**
     * Searches for all the rentals the given book has
     * @param {Book} book - the book to look for
     * @return {Promise<Rentals>}
     */
    async findBookRentals(book, filters) {
        return this.filter({
            book: book.getIdentifier(),
            ...filters
        });
    }

    /**
     * @inheritDoc
     */
    async filter(filters) {
        return super.filter(filters).then(result => {
            const hydrateDependencies = result.map(r => {
                return Promise.all([
                    r.get('book').fetch(),
                    r.get('library').fetch(),
                    r.get('rentee').fetch()
                ]);
            });

            return Promise.all(hydrateDependencies);
        }).then(() => {
            return this;
        });
    }
}

module.exports = Rentals;