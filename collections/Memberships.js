const Collection = require('./Collection');
const Membership = require('../models/Membership');

class Memberships extends Collection {
    /**
     * The model associated with this collection
     * @return {Membership}
     */
    static get model() {
        return Membership;
    }
}

module.exports = Memberships;