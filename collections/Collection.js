const mongo = require('../services/mongoService');

class Collection {

    /**
     * @constructor
     * @param {Model[]} models - array of models
     */
    constructor(models = []) {
        this.models = models;
        this.metadata = {};
    }

    /**
     * Returns the length of the collection
     * @return {number}
     */
    get length() {
        return this.models.length;
    }

    /**
     * Adds a model to the collection
     * @param {Model} model - the model to be added
     */
    push(model) {
        this.models.push(model);
    }

    /**
     * Returns the model at index
     * @param {number} idx
     * @return {Model}
     */
    at(idx) {
        return this.models[idx] || null;
    }

    /**
     * Formats the filters so that they're appropiate to be send to mongo
     * @param {Object} filters - filters to format
     * @return {Object}
     */
    formatFilters(filters) {
        return filters;
    }

    /**
     * Fetches all entries of the collection only taking into account the limit and the offset passed
     * @param {Object} filters - the applied filters
     * @return {Promise<Collection>} - promise resolving with the collection object
     */
    filter(filters = {}) {
        const limit = filters.limit || CONSTANTS.DEFAULT_LIMIT;
        const offset = filters.offset || CONSTANTS.DEFAULT_OFFSET;
        delete filters.limit;
        delete filters.offset;

        filters = this.formatFilters(filters);

        return this.getMongo().find(filters)
            .limit(limit)
            .skip(offset)
            .toArray().then(collection => {
                collection.map(modelData => {
                    return new this.constructor.model(modelData);
                }).forEach(this.push.bind(this));

                return this.getMongo().countDocuments(filters.data);
            }).then(count => {
                this.setMetadata({count});
                return this.getMongo().find(filters.data)
                    .skip(limit + offset).toArray();
            }).then(nextPage => this.setMetadata({
                next: nextPage.length > 0 && limit + offset !== 0,
                prev: offset > 0
            }));
    }

    /**
     * Counts how many models match the criteria callback
     * @param {function} callback - the criteria callback
     * @return {number}
     */
    count(callback) {
        return this.models.reduce((acc, model, idx) => {
            return acc + Number(callback(model, idx));
        }, 0);
    }

    /**
     * Executes a callback for every item in this collection and returns an array containing the results
     * @param {function} callback - the callback to be executed
     * @return {Array}
     */
    map(callback) {
        return this.models.map(callback);
    }

    /**
     * Retrieves the first model that matches the criteria callback
     * @param {function} callback - the criteria callback
     * @return {Model}
     */
    find(callback) {
        return this.models.find(callback);
    }

    /**
     * Retrieves the mongo object for this collection
     * @return {*}
     */
    getMongo() {
        return mongo().collection(this.constructor.model.collection);
    }

    /**
     * Set the metadata to this collection
     * @param {Object} metadata - the metadata object
     * @return {this}
     */
    setMetadata(metadata) {
        Object.assign(this.metadata, metadata);
        return this;
    }

    /**
     * Retrieve the metadata object
     * @return {Object}
     */
    getMetadata() {
        return this.metadata;
    }

    /**
     * Transforms this collection in a JSON array
     * @param {boolean} withMetadata - if true, include the metadata object in the json
     * @return {{metadata: *, data: *[]}|Array}
     */
    json(withMetadata = false) {
        let jsonObject = null;
        if (withMetadata) {
            jsonObject = {
                data: this.models.map(model => model.json()),
                metadata: this.getMetadata()
            };
        } else {
            jsonObject = this.models.map(model => model.json());
        }

        return jsonObject;
    }
}

module.exports = Collection;