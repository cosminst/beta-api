const Collection = require('./Collection');
const Book = require('../models/Book');
const objectId = require('../formatters/objectId');

class Books extends Collection {
    /**
     * The model associated with this collection
     * @return {Book}
     */
    static get model() {
        return Book;
    }

    /**
     * @inheritDoc
     */
    formatFilters(filters) {
        const formatted = Object.assign({}, filters);

        if (filters.title) {
            formatted.title = new RegExp(`.*${filters.title}.*`, 'i');
        }

        if (filters.library) {
            formatted['libraries.library'] = objectId(filters.library);
        }

        delete formatted.library;
        return formatted;
    }

    countByLibrary(library) {
        return this.getMongo().countDocuments({
            'libraries.library': library.getIdentifier()
        });
    }

    /**
     * @inheritDoc
     */
    filter(filters = {}) {
        return super.filter(filters).then(collection => {
            const hydrateBooks = collection.map(book => {
                const fetchLibraries = book.get('libraries').map(library => {
                    return library.fetch();
                });

                return Promise.all(fetchLibraries);
            });

            return Promise.all(hydrateBooks);
        }).then(() => this);
    }
}

module.exports = Books;