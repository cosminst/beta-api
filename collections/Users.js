const Collection = require('./Collection');
const User = require('../models/User');

class Users extends Collection {
    static get model() {
        return User;
    }
}

module.exports = Users;